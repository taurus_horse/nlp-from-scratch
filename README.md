本文件夹主要记录学习书籍《深度学习进阶：自然语言处理》过程中的代码，本书没有使用pytorch框架，是从零开始使用python编写的代码。

书籍提供的源代码为python文件，学习时用的是jupyter notebook。

+ nlpscratch-20230208T034637Z-001:在colab上训练的文件打包下载
+ source code： 书籍提供的python文件源代码

## jupyter ：自己编写的jupyter notebook代码

| 文件夹 | 说明                           |
| ------ | ------------------------------ |
| ch01   | 神经网络的复习                 |
| ch02   | 自然语言处理与单词的分布式表示 |
| ch03   | word2vec                       |
| ch04   | word2vec的高速化               |
| ch05   | RNN                            |
| ch06   | GatedRNN(LSTM)                 |
| ch07   | seq2seq                        |
| ch08   | Attention                      |

**Ipynb_importer.py** 

文件可以在ipynb文件中引用其他ipynb中定义的函数与类

使用时

> import Ipynb_importer
>
> import [ipynb 定义的module]

**transform.py**

为了将ipynb转换成markdown而写的转换脚本。如果使用jupyter notebook或者jupyter lab的import功能导出md，那么使用剪贴板粘贴的图片文件在md中会显示成attachment，具体见博客文章[jupyter中复制粘贴形式插入的图片(attachment)导出md时不显示_jinniulema的博客-CSDN博客](https://blog.csdn.net/jinniulema/article/details/128858938)

**requirements.txt文件由conda命令自动导出**

> conda list -e > requirements.txt

安装命令

> conda install --yes --file requirements.txt #这种执行方式，一遇到安装不上就整体停止不会继续下面的包安装

如果requirements.txt中的包不可用，则会抛出“无包错误”

使用下面这个命令可以解决这个问题

> $ while read requirement; do conda install --yes $requirement; done < requirements.txt

如果想要在[conda命令](https://so.csdn.net/so/search?q=conda命令&spm=1001.2101.3001.7020)无效时使用pip命令来代替，那么使用如下命令：

> $ while read requirement; do conda install --yes $requirement || pip install $requirement; done < requirements.txt

**freeze.yml也为自动导出**

> conda env export > freeze.yml

使用

> conda env create -f freeze.yml
