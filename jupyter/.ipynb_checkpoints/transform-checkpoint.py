"""
将jupyter 的 markdown单元格中以复制粘贴形式插入的图片转换成图床图片
![image.png](attachment:cc48e68c-db89-4481-9f84-9900c4759344.png)  ----> ![png](https://***.png)
""" 
import os
import re
import json
import fire
import base64
import requests

headers = {'Content-Type':'application/json'}

    
def get_json_data(filename):
    dicts = {}
    with open(filename, 'r', encoding = 'utf-8') as f:
        ipynb_content = json.load(f)
        cells = ipynb_content["cells"]
        for cell in cells:
            sources = cell['source']
            if "attachments" in cell:
                attachments = cell['attachments']
                for idx, source in enumerate(sources):
                    attachment = 0
                    while(True):
                        attachment = source.find('attachment:', attachment)
                        if attachment==-1:
                            break
                        imageidx = attachment + 11
                        rbracket = source.find(')', imageidx)
                        imagename = source[imageidx:rbracket]
                        print(imagename)
                        value = attachments[imagename]
                        for data_type, base64data in value.items(): # 其实应该就一个key，valule
                            if "image" in data_type:  # image/png  image/jpeg
                                # print (key, data_type, base64data[:10])
                                pic_data = base64.b64decode(base64data)
                                if "png" in data_type:
                                    with open('temp.png', 'wb')as f:
                                        f.write(pic_data)
                                        cur_path = os.path.abspath('./temp.png')
                                if "jpeg" in data_type:
                                    with open('temp.jpeg', 'wb') as f:
                                        f.write(pic_data)
                                        cur_path = os.path.abspath('./temp.jpeg')
                                datas = json.dumps({'list': [cur_path]})
                                response = requests.post('http://127.0.0.1:36677/upload', data=datas, headers=headers)
                                result = response.json()['result']
                                # print(result[0])

                                url = result[0]
                                print(url)

                                print(source[attachment:rbracket])
                                sources[idx] = source.replace(source[attachment:rbracket],url)
                                # print(sources[idx])
                        attachment = rbracket + 1
                cell.pop('attachments')
                # print(cell)
        ipynb_content['cells'] = cells
        # dicts = ipynb_content
    with open(filename, 'w', encoding = 'utf-8') as w:
        # print(dicts)
        json.dump(ipynb_content, w, indent = 1, ensure_ascii=False)
        
if __name__ == '__main__':
    fire.Fire(get_json_data)
    
    