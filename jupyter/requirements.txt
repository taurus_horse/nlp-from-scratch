# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: win-64
blas=1.0=mkl
bleach=1.5.0=py36_0
certifi=2022.12.7=pypi_0
charset-normalizer=2.0.12=pypi_0
click=8.0.4=pypi_0
colorama=0.3.9=py36_0
cycler=0.10.0=py36_0
decorator=4.1.2=py36_0
entrypoints=0.2.3=py36_0
fire=0.5.0=pypi_0
html5lib=0.9999999=py36_0
icu=57.1=vc14_0
idna=3.4=pypi_0
importlib-metadata=4.8.3=pypi_0
importlib-resources=5.4.0=pypi_0
ipykernel=4.6.1=py36_0
ipython=6.1.0=py36_0
ipython_genutils=0.2.0=py36_0
jdc=0.0.9=pypi_0
jedi=0.17.2=pypi_0
jinja2=2.9.6=py36_0
joblib=1.1.1=pypi_0
jpeg=9b=vc14_0
jsonschema=2.6.0=py36_0
jupyter-lsp=0.9.3=pyhd8ed1ab_0
jupyter_client=5.1.0=py36_0
jupyter_core=4.3.0=py36_0
libpng=1.6.30=vc14_1
markupsafe=1.0=py36_0
matplotlib=2.0.2=np113py36_0
mistune=0.7.4=py36_0
mkl=2017.0.3=0
nbconvert=5.2.1=py36_0
nbformat=4.4.0=py36_0
nltk=3.6.7=pypi_0
notebook=5.0.0=py36_0
numpy=1.13.1=py36_0
openssl=1.0.2l=vc14_0
pandoc=2.3=pypi_0
pandocfilters=1.4.2=py36_0
parso=0.7.1=pypi_0
pickleshare=0.7.5=py_1003
pip=9.0.1=py36_1
pluggy=1.0.0=pypi_0
plumbum=1.8.1=pypi_0
ply=3.11=pypi_0
prompt_toolkit=1.0.15=py36_0
pygments=2.2.0=py36_0
pyparsing=2.2.0=py36_0
pyqt=5.6.0=py36_2
python=3.6.2=0
python-dateutil=2.6.1=py36_0
python-jsonrpc-server=0.4.0=pypi_0
python-language-server=0.36.2=pypi_0
pytz=2017.2=py36_0
pywin32=305=pypi_0
pyzmq=16.0.2=py36_0
qt=5.6.2=vc14_6
regex=2022.10.31=pypi_0
requests=2.27.1=pypi_0
setuptools=27.2.0=py36_1
simplegeneric=0.8.1=py36_1
sip=4.18=py36_0
six=1.10.0=py36_0
sklearn=0.0.post1=pypi_0
termcolor=1.1.0=pypi_0
testpath=0.3.1=py36_0
tk=8.5.18=vc14_0
tornado=4.5.2=py36_0
tqdm=4.64.1=pypi_0
traitlets=4.3.2=py36_0
typing-extensions=4.1.1=pypi_0
ujson=4.3.0=pypi_0
urllib3=1.26.14=pypi_0
vc=14=0
vs2008_runtime=9.00.30729.5054=0
vs2015_runtime=14.0.25420=0
wcwidth=0.1.7=py36_0
wheel=0.29.0=py36_0
zipp=3.6.0=pypi_0
zlib=1.2.11=vc14_0
